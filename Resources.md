[TOC]
# .NET资源分类

1. 嵌入资源

2. 内容资源

程序集中的代码需要访问磁盘上的文件时，可将文件嵌入进程序集，或将文件的相对路径嵌入进程序集；

程序集执行到访问文件的代码时，前者去本程序集寻找资源，称嵌入资源；后者在本程序集拿到文件的相对路径，然后去寻找加载文件，称内容资源。

# 嵌入资源和内容资源的优缺点比较

嵌入资源：资源被嵌入进程序集，从而资源和访问资源的代码都在同一程序集中，资源和代码绑定在一起，程序集永远不会丢失资源，不会出现找不到资源的情况，但资源嵌入程序集会迅速增大程序集的体积，增大加载程序集的时间。
内容资源：资源的相对路径被嵌入到程序集中，但资源本身并没有嵌入程序集。编译时，资源同它们的目录结构一起会被自动的拷贝到应用程序的根目录。应用程序执行时，一般都设置当前工作目录是应用程序根目录，所以代码中的相对路径，也是相对应用程序的根目录，故编译时的自动拷贝内容资源到应用程序根目录，也是Visual Studio的一种“默契”。

# 嵌入资源

## 利用.resx文件嵌入资源

xxx.resx是一种具有特定格式的XML文件，按照规则在文件中填入资源的路径等信息，resgen.exe作用于xxx.resx文件，根据XML中的路径找到资源，然后将资源转换成xxx.resources二进制文件，最后csc.exe将xxx.resources和cs文件编译成程序集。资源在二进制文件中的组织形式像字典，key是资源名称，value是资源的字节数组形式。根据资源名称，能在程序集的资源段的.resources中，检索到资源。

### 添加嵌入资源

有两种添加方式，第一种是比较原始的方式，第二种是利用Visual Studio提供的便捷方式。

#### 方式一：手工编辑xxx.resx

假设我们将图片card.jpg和一些字符串嵌入进程序集作为嵌入资源。

1. 在D:\asm\新建xxx.rex
2. 在D:\asm\新建文件夹new_dir，并在new_dir下放入图片card.jpg
3. 按照.NET预定的格式编辑xxx.rex，主要是填入路径信息

```xml
  <data name="btn1" xml:space="preserve">
    <value>打开</value>
  </data>
  <data name="btn2" xml:space="preserve">
    <value>关闭</value>
  </data>
  <assembly alias="System.Windows.Forms" name="System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
  <data name="idCardIcon" type="System.Resources.ResXFileRef, System.Windows.Forms">
    <value>new_dir\card.jpg;System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </data>
```

4. 运行resgen.exe  xxx.resx，会生成xxx.resources。这一步，在xxx.resx中按照路径找到资源【假设当前的工作目录是new_dir的父目录，会正常的去new_dir下找到card.jpg；如果xml中配置的value是card.jpg，而不是new_dir\card.jpg，则会找不到card.jpg，除非当前的工作目录切换成new_dir】，然后提取所有的资源转换成.resources二进制文件。注意，二进制文件中并不包含资源的路径和文件名信息，只是简单的【资源key名 --- 资源二进制】，使用资源时，通过key拿到一个二进制块。
5. csc.exe /resource:xxx.resources Program.cs，将资源和cs源代码文件编译成程序集。

#### 方式二：可视化编辑xxx.resx

方式一有两点不便，xxx.resx的格式是有限定的，容易填错；还要敲命令编译.resources，难度太高。所以，visual studio提供了可视化编辑resx文件的方法。

1. 添加资源文件
   
   ![2021-03-06_031850.png](https://i.loli.net/2021/03/06/XtP1cVJ8TyeWID5.png)

2. 项目会多出两个文件

![2.png](https://i.loli.net/2021/03/06/RQYdN2A1tznJv8k.png)

3. 双击xxx.resx，打开可视化画面，增删资源

![3.png](https://i.loli.net/2021/03/06/JEIobyBlAztv93S.png)

4. 添加资源后，项目会自动将资源拷贝到Resources文件夹

![4.png](https://i.loli.net/2021/03/06/7FH15AtGBlbCRms.png)

5. 设置资源属性为【不复制】。因为资源被嵌入进程序集，程序也从程序集内部寻找资源，所以没必要把资源拷贝到根目录下。

   <img src="https://i.loli.net/2021/03/06/cqptgdv3i8J1eQL.png" title="" alt="123.png" data-align="inline">

   6. 编译

   编译时的工作目录是项目的一级目录，所以编译时，能找到资源和源代码。

   

   

   编译完成后，资源共存在4个位置

（1）资源原来所在的位置

（2）项目目录结构下的Resources文件夹

（3）ResxResourcesDemo.xxx.resources（本应是xxx.resources，但是.NET自己加了手脚，就是.resources默认追加上项目的默认命名空间）

（4）程序集内部

* 编译完成后，我们可以删除位置（1）（2）（3）的资源，程序集也能正常工作。但是我们重新编译程序集，会重新检索.resx，搜索Resources文件夹下的资源，重新生成.resources并嵌入。所以我们增删资源【一定】要通过可视化.resx，如果只是简单的在Resources文件夹下增删，重新编译，解析.resx的资源路径，会因为找不到资源导致出错！

#### 强调

* 一个.resx生成一个.resources文件
* .resx只包含资源的路径信息，.resources包含真正的资源并嵌入进程序集
* 一个项目可以包含多个.rex，所以一个程序集可以嵌入多个.resources
* 多个.resx的资源会被统一拷贝到Resource文件夹下
* 因为所有的.resx的资源都会被拷贝到Resource文件夹下，如果资源很多，想分类管理，那么可以通过新建多个.resx达到分类的目的
* .resources二进制资源被嵌入到程序集的资源段
* .resources文件名是 项目默认命名空间 + . + .resx文件名 + .resources
* 访问资源需要的信息包括：资源所在程序集，资源所在的.resources名，资源的key名

### 访问嵌入资源

```csharp
/* 读取指定程序集中的指定.resources(.resx)的资源 */

// 1.命名空间.(resx文件名 | .resource文件名) 2.程序集
ResourceManager resource = new ResourceManager("MyNamespace.xxx", Assembly.GetExecutingAssembly());
// 读取字符串或文本
string str = resource.GetString("str");
// 读取图片
Bitmap bitmap = resource.GetObject("pic") as Bitmap;
// 读取其他文件为字节数组
UnmanagedMemoryStream stream = resource.GetStream("file");
byte[] bytes = new byte[1024];
stream.Read(bytes, 0, 1024);
stream.Dispose();
```

```csharp
// 遍历一个程序集内的所有资源
string[] resNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
foreach (string resName in resNames)
{
    ResourceManager resource1 = new ResourceManager("MyNamespace.xxx", Assembly.GetExecutingAssembly());
    using (ResourceSet set = resource1.GetResourceSet(CultureInfo.CurrentCulture, true, true))
    {
        foreach (DictionaryEntry res in set)
        {
            Console.WriteLine(res.Key);
            Console.WriteLine(res.Value);
        }
    }
}
```

我们注意到.NET还为每个.resx自动生成一个xxx.Designer.cs，该文件只是封装了ResourceManager访问资源的API，使我们更加容易的访问资源，不必自己再写一堆代码。

![1.png](https://i.loli.net/2021/03/06/ZzEgPIt8JBT4dxm.png)

![3.png](https://i.loli.net/2021/03/06/Hmy63bc25UkTtJn.png)

![4.png](https://i.loli.net/2021/03/06/xOaW7qfjAvdIGEo.png)

类名是.resx文件名，资源作为静态成员存在，权限为internal，我们可以修改权限，可以让程序集的资源能被其他程序集访问。

修改权限的方法：

![1.png](https://i.loli.net/2021/03/06/JOsI5chjDBbxKkw.png)

## 利用uri机制嵌入资源

# 卫星程序集
