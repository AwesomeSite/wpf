# 什么是MVVM

MVVM

M: Model,业务逻辑层，对现实世界的抽象类

V: View，即UI，显示Model层的变量，调用Model层的方法

VM: View-Model,Model For View

View和ViewModel的沟通：

​	传递数据（变量）：绑定数据属性

​	传递操作（方法）：绑定命令属性

# MVVM的好处

团队：统一思路，快速理解上手项目结构

架构：UI和业务逻辑解耦

代码：不修改业务逻辑层代码，轻松更换应用皮肤；方便实现单元测试



